import { configure, getLogger } from "log4js";
import path from "path";
import utils from "./utils";

import { BUILD_FOLDER, LOGS_FOLDER } from "./../configs/config.json";

const rootPath = path.resolve(process.cwd(), utils.getBuildFolder(), LOGS_FOLDER);

// better to log errors at error-logs.log, another logs at logs.log
configure({
    appenders: {
        everything: { type: "file", filename: `${rootPath}/logs.log` }
    },
    categories: { default: { appenders: ["everything"], level: "debug" } }
});

export default getLogger();
