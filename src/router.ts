import logger from "./logger";
import services from "./services";

interface RouterRequestData {
    eventName: string;
    data: any;
}

export default function(io?: any): Function {
    return function(socket: any): void {
        const { headers, address } = socket.handshake;
        const userAgent = headers["user-agent"];
        const userLogString = `${address} ${userAgent}`;

        logger.info(`${userLogString} - New connection`);

        socket.on("router", ({ eventName, data }: RouterRequestData) => {
            const event = services()[eventName];
            if (event) event(io, data, userLogString);
        });
    };
}
