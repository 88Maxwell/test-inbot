import fs from "fs";
import path from "path";
import logger from "../logger";
import utils from "../utils";
import { IMAGE_FOLDER, PUBLIC_FOLDER, HOST, PORT } from "../../configs/config.json";

export default (io: any, buffer: Buffer, userLogString: string) => {
    const fileName = `${utils.getHash()}.jpeg`;
    const filePath = path.resolve(process.cwd(), utils.getBuildFolder(), PUBLIC_FOLDER, IMAGE_FOLDER, fileName);
    const publicFileURL = `http://${HOST}:${PORT}/${IMAGE_FOLDER}/${fileName}`;

    fs.writeFile(filePath, buffer, () => {
        logger.info(`${userLogString} - New image: ${publicFileURL}`);
    });
};
