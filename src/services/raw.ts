import logger from "../logger";

export default (io: any, raw: string, userLogString: string) => {
    logger.info(`${userLogString} - New RAW: ${raw}`);
};
