import raw from "./raw";
import json from "./json";
import file from "./file";

interface Services {
    [name: string ]: Function;
}

export default (): Services => ({ raw, json, file });
