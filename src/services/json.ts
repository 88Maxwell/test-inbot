import { google }  from "googleapis";
import logger from "../logger";
import config from "../../configs/config.json";

export default (io: any, jsonData: string, userLogString: string) => {
    const data = JSON.parse(jsonData);

    if (data && data.query) {
        const customsearch = google.customsearch("v1");
        const { KEY, CX } = config.GOOGLE_SEARCH;
        customsearch.cse
            .list({
                auth: KEY,
                cx: CX,
                q: data.query,
            })
            .then(result => result.data)
            .then((result) => {
                const page = (result.queries.request || [])[0] || {};

                logger.info(
                    `${userLogString} - New json: ${jsonData}, About, ${
                        page.totalResults
                    } results`,
                );
            })
            .catch(() => logger.error(
                `${userLogString} - New json: ${jsonData}, can\`t get a count of result`,
            ));
    }
};
