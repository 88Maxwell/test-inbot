import crypto from "crypto";
import { BUILD_FOLDER, BUILD_FOLDER_DEV } from "../configs/config.json";

export default {
    getHash: (): string =>
        crypto
            .createHash("sha1")
            .update(Date.now().toString() + Math.random().toString())
            .digest("hex"),

    generateQueryString: (url: string, queryParams: any): string =>
        `${url}?${Object.keys(queryParams)
            .map(key => `${key}=${queryParams[key]}`)
            .join("&")}`,
    getBuildFolder: (): string => process.env.NODE_ENV === "production" ? BUILD_FOLDER : BUILD_FOLDER_DEV
};
