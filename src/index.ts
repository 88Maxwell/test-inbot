import express from "express";
import http from "http";
import path from "path";
import socketIO from "socket.io";

import {  HOST, PORT, PUBLIC_FOLDER } from "../configs/config.json";
import utils from "./utils";
import router from "./router";

const app = express();


app.use("/", express.static(path.resolve(process.cwd(), utils.getBuildFolder(), PUBLIC_FOLDER)));

const server = new http.Server(app);
const io = socketIO(server);

io.on("connection", router());

// eslint-disable-next-line no-console
server.listen(PORT, (): void => console.log(`SERVER START AT PORT: http://${HOST}:${PORT}`));
